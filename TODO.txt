# RESTRUCTURING TODOs

- write a proper setup.sh script and provide installer instructions (`pip install -e .` recommended)

- use proper data-file lookup in the install location for the db, analysis lists, etc.

- replace the executable shebang #! with -*- python -*- at the top of .py sources

- allow pytest running from the top dir
