contur.data package
===================

Submodules
----------

contur.data.build\_covariance module
------------------------------------

.. automodule:: contur.data.build_covariance
   :members:
   :undoc-members:
   :show-inheritance:

contur.data.static\_db module
-----------------------------

.. automodule:: contur.data.static_db
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: contur.data
   :members:
   :undoc-members:
   :show-inheritance:
