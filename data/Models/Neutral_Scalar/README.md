# Two simplified Neutral Scalar Models

The models used in Contribution 20 of these Les Houches Proceedings https://arxiv.org/abs/1803.10379

##  Model Authors: Sylvain Fichet, Gregory Moreau

See also 

https://contur.hepforge.org/results/lightscalarLH/index.html

