# This file was automatically created by FeynRules 2.0.33
# Mathematica version: 7.0 for Mac OS X x86 (64-bit) (February 19, 2009)
# Date: Tue 19 Jan 2016 17:07:08


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NEW = CouplingOrder(name = 'NEW',
                    expansion_order = 99,
                    hierarchy = 3)

HEFT = CouplingOrder(name = 'HEFT',
                     expansion_order = 99,
                     hierarchy = 4)

