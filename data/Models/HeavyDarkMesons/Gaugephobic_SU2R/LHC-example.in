read FRModel.model

set /Herwig/FRModel/FRModel:NDark {NDark}
set /Herwig/FRModel/FRModel:PionMass {PionMass}
set /Herwig/FRModel/FRModel:FermionEta {FermionEta}
set /Herwig/FRModel/FRModel:VPi {VPi}
set /Herwig/FRModel/FRModel:Xi {Xi}

####################################
#
# Modify the required process here
#
####################################

cd /Herwig/NewPhysics

## turning on gluons and all quarks except top as incoming
insert HPConstructor:Incoming 0 /Herwig/Particles/u
insert HPConstructor:Incoming 0 /Herwig/Particles/ubar
insert HPConstructor:Incoming 0 /Herwig/Particles/d
insert HPConstructor:Incoming 0 /Herwig/Particles/dbar
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 0 /Herwig/Particles/s
insert HPConstructor:Incoming 0 /Herwig/Particles/sbar
insert HPConstructor:Incoming 0 /Herwig/Particles/b
insert HPConstructor:Incoming 0 /Herwig/Particles/bbar
insert HPConstructor:Incoming 0 /Herwig/Particles/c
insert HPConstructor:Incoming 0 /Herwig/Particles/cbar

## turning on all the new particles as possible outgoing
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/rho0_UFO
# insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/rho+_UFO
# insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/rho-_UFO
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/DP+
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/DP-
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/DP0

set HPConstructor:Processes SingleParticleInclusive


#############################################################
## Additionally, you can use new particles as intermediates
## with the ResConstructor:
#############################################################
insert ResConstructor:Incoming 0 /Herwig/Particles/u
insert ResConstructor:Incoming 0 /Herwig/Particles/ubar
insert ResConstructor:Incoming 0 /Herwig/Particles/d
insert ResConstructor:Incoming 0 /Herwig/Particles/dbar
insert ResConstructor:Incoming 0 /Herwig/Particles/c
insert ResConstructor:Incoming 0 /Herwig/Particles/cbar
insert ResConstructor:Incoming 0 /Herwig/Particles/s
insert ResConstructor:Incoming 0 /Herwig/Particles/sbar
insert ResConstructor:Incoming 0 /Herwig/Particles/b
insert ResConstructor:Incoming 0 /Herwig/Particles/bbar

## turning on all the BSM rho0 as a possible resonance (intermediate particle)
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/rho0_UFO

insert ResConstructor:Outgoing 0 /Herwig/Particles/u
insert ResConstructor:Outgoing 0 /Herwig/Particles/ubar
insert ResConstructor:Outgoing 0 /Herwig/Particles/d
insert ResConstructor:Outgoing 0 /Herwig/Particles/dbar
insert ResConstructor:Outgoing 0 /Herwig/Particles/c
insert ResConstructor:Outgoing 0 /Herwig/Particles/cbar
insert ResConstructor:Outgoing 0 /Herwig/Particles/s
insert ResConstructor:Outgoing 0 /Herwig/Particles/sbar
insert ResConstructor:Outgoing 0 /Herwig/Particles/b
insert ResConstructor:Outgoing 0 /Herwig/Particles/bbar
insert ResConstructor:Outgoing 0 /Herwig/Particles/t
insert ResConstructor:Outgoing 0 /Herwig/Particles/tbar


###########################################################
# Specialized 2->3 higgs constructors are also available,
# where incoming lines don't need to be set.
###########################################################
## Higgs + t tbar
# set /Herwig/NewPhysics/QQHiggsConstructor:QuarkType Top
# insert /Herwig/NewPhysics/QQHiggsConstructor:HiggsBoson  0 [HIGGS_NAME]
#
## Higgs VBF
# insert /Herwig/NewPhysics/HiggsVBFConstructor:HiggsBoson  0 [HIGGS_NAME]
#
## Higgs + W/Z, with full 2->3 ME
# set /Herwig/NewPhysics/HVConstructor:CollisionType Hadron
# insert /Herwig/NewPhysics/HVConstructor:VectorBoson 0 /Herwig/Particles/Z0
# insert /Herwig/NewPhysics/HVConstructor:HiggsBoson  0 [HIGGS_NAME]

####################################
####################################
####################################
read snippets/PPCollider.in

# Intrinsic pT tune extrapolated to LHC energy
set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV

# disable default cuts if required
# cd /Herwig/EventHandlers
# create ThePEG::Cuts   /Herwig/Cuts/NoCuts
# set EventHandler:Cuts /Herwig/Cuts/NoCuts

# Other parameters for run
cd /Herwig/Generators
set EventGenerator:NumberOfEvents 10000000
set EventGenerator:RandomNumberGenerator:Seed 31122001
set EventGenerator:DebugLevel 0
set EventGenerator:EventHandler:StatLevel Full
set EventGenerator:PrintEvent 100
set EventGenerator:MaxErrors 10000


