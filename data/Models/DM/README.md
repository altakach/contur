# Simplified Dark Matter models

*  The model used in the first Contur paper https://arxiv.org/abs/1606.05296 is here:
[DM_vector_mediator_UFO](DM_vector_mediator_UFO)
This model couples only to first generation quarks. 
See [here](https://contur.hepforge.org/results/simplevdm1g/index.html) for latest results.

*  A second model which couples to all three generations is here:
[DM_vector_mediator_HF_UFO](DM_vector_mediator_HF_UFO)
See [here](https://contur.hepforge.org/results/simplevdm3g/index.html) for latest results.

*  LHC DMWG benchmark model
[Simple DM with spin 1 mediator](DMsimp_s_spin1)
See [here](https://contur.hepforge.org/results/dmsimpspin1/index.html) for latest results.

* There are many models available from the LHC Dark Matter Working Group.
https://github.com/LHC-DMWG/model-repository/tree/master/models 

    - Some results for [Pseudoscalar_2HDM](https://github.com/LHC-DMWG/model-repository/tree/master/models/Pseudoscalar_2HDM) are available [here](https://contur.hepforge.org/results/Pseudoscalar_2HDM/index.html).
