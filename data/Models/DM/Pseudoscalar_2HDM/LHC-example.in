read FRModel.model
set /Herwig/FRModel/Particles/Xd:NominalMass {mXd}*GeV
set /Herwig/FRModel/Particles/Xd~:NominalMass {mXd}*GeV
set /Herwig/FRModel/Particles/h2:NominalMass {mh2}*GeV
set /Herwig/FRModel/Particles/h3:NominalMass {mh3}*GeV
set /Herwig/FRModel/Particles/h+:NominalMass {mhc}*GeV
set /Herwig/FRModel/Particles/h-:NominalMass {mhc}*GeV
set /Herwig/FRModel/Particles/h4:NominalMass {mh4}*GeV
set /Herwig/FRModel/FRModel:gPXd {gPXd}
set /Herwig/FRModel/FRModel:tanbeta {tanbeta}
set /Herwig/FRModel/FRModel:sinbma {sinbma}
set /Herwig/FRModel/FRModel:sinp {sinp}
set /Herwig/FRModel/FRModel:lam3 {lam3}
set /Herwig/FRModel/FRModel:laP1 {laP1}
set /Herwig/FRModel/FRModel:laP2 {laP2}

cd /Herwig/Cuts
set JetKtCut:MinKT 50*GeV

####################################
#
# Modify the required process here
#
####################################

cd /Herwig/NewPhysics

insert HPConstructor:Incoming 0 /Herwig/Particles/u
insert HPConstructor:Incoming 0 /Herwig/Particles/ubar
insert HPConstructor:Incoming 0 /Herwig/Particles/d
insert HPConstructor:Incoming 0 /Herwig/Particles/dbar
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 0 /Herwig/Particles/s
insert HPConstructor:Incoming 0 /Herwig/Particles/sbar
insert HPConstructor:Incoming 0 /Herwig/Particles/b
insert HPConstructor:Incoming 0 /Herwig/Particles/bbar
insert HPConstructor:Incoming 0 /Herwig/Particles/c
insert HPConstructor:Incoming 0 /Herwig/Particles/cbar

insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/h+
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/h-
# insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/h1
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/h2
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/h3
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/h4
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/Xd
insert HPConstructor:Outgoing 0 /Herwig/FRModel/Particles/Xd~

set HPConstructor:Processes SingleParticleInclusive

#############################################################
## Additionally, you can use new particles as intermediates
## with the ResConstructor:
#############################################################
insert ResConstructor:Incoming 0 /Herwig/Particles/u
insert ResConstructor:Incoming 0 /Herwig/Particles/ubar
insert ResConstructor:Incoming 0 /Herwig/Particles/d
insert ResConstructor:Incoming 0 /Herwig/Particles/dbar
insert ResConstructor:Incoming 0 /Herwig/Particles/s
insert ResConstructor:Incoming 0 /Herwig/Particles/sbar
insert ResConstructor:Incoming 0 /Herwig/Particles/c
insert ResConstructor:Incoming 0 /Herwig/Particles/cbar
insert ResConstructor:Incoming 0 /Herwig/Particles/b
insert ResConstructor:Incoming 0 /Herwig/Particles/bbar
insert ResConstructor:Incoming 0 /Herwig/Particles/g

insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/h+
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/h-
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/h2
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/h3
insert ResConstructor:Intermediates 0 /Herwig/FRModel/Particles/h4

insert ResConstructor:Outgoing 0 /Herwig/Particles/W-
insert ResConstructor:Outgoing 1 /Herwig/Particles/W+
insert ResConstructor:Outgoing 2 /Herwig/Particles/Z0
insert ResConstructor:Outgoing 3 /Herwig/Particles/mu+
insert ResConstructor:Outgoing 3 /Herwig/Particles/mu-
insert ResConstructor:Outgoing 3 /Herwig/Particles/e+
insert ResConstructor:Outgoing 3 /Herwig/Particles/e-
insert ResConstructor:Outgoing 3 /Herwig/Particles/tau+
insert ResConstructor:Outgoing 3 /Herwig/Particles/tau-
insert ResConstructor:Outgoing 3 /Herwig/Particles/u
insert ResConstructor:Outgoing 3 /Herwig/Particles/ubar
insert ResConstructor:Outgoing 3 /Herwig/Particles/d
insert ResConstructor:Outgoing 3 /Herwig/Particles/dbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/s
insert ResConstructor:Outgoing 3 /Herwig/Particles/sbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/c
insert ResConstructor:Outgoing 3 /Herwig/Particles/cbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/b
insert ResConstructor:Outgoing 3 /Herwig/Particles/bbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/t
insert ResConstructor:Outgoing 3 /Herwig/Particles/tbar
insert ResConstructor:Outgoing 3 /Herwig/Particles/gamma


####################################
####################################
####################################
read snippets/PPCollider.in

# Intrinsic pT tune extrapolated to LHC energy
set /Herwig/Shower/ShowerHandler:IntrinsicPtGaussian 2.2*GeV

# disable default cuts if required
# cd /Herwig/EventHandlers
# create ThePEG::Cuts   /Herwig/Cuts/NoCuts
# set EventHandler:Cuts /Herwig/Cuts/NoCuts

# Other parameters for run
cd /Herwig/Generators
set EventGenerator:NumberOfEvents 10000000
set EventGenerator:RandomNumberGenerator:Seed 31122001
set EventGenerator:DebugLevel 0
set EventGenerator:EventHandler:StatLevel Full
set EventGenerator:PrintEvent 100
set EventGenerator:MaxErrors 10000



