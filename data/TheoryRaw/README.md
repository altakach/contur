Directories containing the data from which the Theory reference (yoda) plots are made
-------------------------------------------------------------------------------------

(unless they come straight from HEPDATA or were read manually directly into yoda)

NNLO-Photons contains predictions from 
Isolated photon and photon+jet production at NNLO QCD accuracy
Xuan Chen, Thomas Gehrmann, Nigel Glover, Marius Höfer, Alexander Huss
arXiv:1904.01044

ATLAS_2012_I1199269
Data grabbed from paper using Datathief
