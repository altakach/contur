SHELL := /bin/bash

DBDIR = data/DB
ANALYSES = data/Rivet
GRIDPACK = data/share

all : $(DBDIR)/analyses.db $(ANALYSES)/Rivet-ConturOverload.so $(GRIDPACK)

$(ANALYSES)/Rivet-ConturOverload.so : $(ANALYSES)/*
	$(ANALYSES)/buildrivet.sh

.PHONY : $(DBDIR)/analyses.db
$(DBDIR)/analyses.db : $(DBDIR)/analyses.sql
	rm -f $@
	sqlite3 $@ < $<

.PHONY : $(GRIDPACK)
$(GRIDPACK) :
	rm -rf $(GRIDPACK)
	mkdir $(GRIDPACK)
	contur-mkana -o $(GRIDPACK)
	mv $(GRIDPACK)/*.rst doc/
	@echo "INFO: You should source setupContur.sh again to update environment variables"

.PHONY : doc
doc :
	cd doc && make html
	@echo "HTML docs built at doc/_build/html/index.html"

.PHONY : check
check :
	cd tests && pytest

.PHONY : clean
clean:
	@echo "Removing all generated files"
	@rm -f contur/*.pyc
	@rm -f contur/*/*.pyc
	@rm -f data/DB/analysis.db
	@rm -f data/Rivet/*.so
	@rm -rf data/GridSetup.bak/GridPack
