#!/bin/bash

source /contur/activate-environment.sh \
    && cd /contur/contur \
    && source ./setupContur.sh \
    && make \
    && source ./setupContur.sh

/bin/bash