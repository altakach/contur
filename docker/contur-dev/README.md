# Contur Docker Dev Env

This directory contains a Docker set-up that enables you to test and debug
modifications to Contur source directly from your machine, in real time. You
don't need to copy the directory every time and wait for installation
(unless there are changes in the dependencies or Linux setup); simply run
`docker-compose run contur sh` from this directory and Docker Compose will launch a
container that loads contur as a shared volume with your local machine, so that
any changes in source are immediately replicated in the container.

**Note**: the first time this environment is used, you need to build the
Docker image first (by running `./build-dev/.sh`. Subsequent times, if Docker
image cache is not cleared and no dependency changes are made, you can simply
start the container directly without having to build the image again.

----

If you run `docker-compose up`, Docker will execute the
entrypoint and exit with code 0. This is because the entrypoint simply calls
the `setupContur.sh` file (and `make` as well). If you want to 'SSH' into the
container and keep it alive, you need to run `docker-compose run contur sh`.

You can replace the entrypoint with custom files that execute test suites
 with little modification.