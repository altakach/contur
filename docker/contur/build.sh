#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.3

# IMPORTANT: put 'latest' version last in the list: it gets reused after the loop
for CONTUR_BRANCH in master contur-2.0.0; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}
    for PYTHON_VERSION in py2 py3; do

        MSG="Building contur with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION, Python=$PYTHON_VERSION"
        tag="hepstore/contur:$CONTUR_VERSION-$PYTHON_VERSION"

        docker build . -f Dockerfile \
               --build-arg RIVET_VERSION=$RIVET_VERSION \
               --build-arg PYTHON_VERSION=$PYTHON_VERSION \
               --build-arg CONTUR_BRANCH=$CONTUR_BRANCH \
               -t $tag

        if [[ "$PUSH" = 1 ]]; then
            docker push $tag && sleep 30s
        fi
    done

    docker tag hepstore/contur:$CONTUR_VERSION-py3 hepstore/contur:$CONTUR_VERSION
    if [[ "$PUSH" = 1 ]]; then
        docker push hepstore/contur:$CONTUR_VERSION && sleep 30s
    fi

done

if [[ "$LATEST" = 1 ]]; then
    docker tag hepstore/contur:$CONTUR_VERSION-py3 hepstore/contur:latest
    if [[ "$PUSH" = 1 ]]; then
        docker push hepstore/contur:latest
    fi
fi
