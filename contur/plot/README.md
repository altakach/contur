# contur/contur/Plotting

## Plotting

Tools for making heatmaps and contour plots from map files. The main script is `contur-plot`.
See
```
$ contur-plot --help
```
for command line guidance.

Additional contours can be superimposed, to indicate previous limits or theory
constraints. These should be supplied in a Python script which is then passed
(without the .py extension) via the `-d` command line option.

The script takes a parameter dictionary as input and returns a list of parameter
points (with parameter keys based on the input dictionary) and values which are
zero or positive if the point is excluded, negative otherwise.

Examples of such scripts for some studied models are in the
[Models](../../../../Models) directory, for example [this
one](../../../../Models/VLQ/flavour.py) for flavour constraints on a VLQ grid.
